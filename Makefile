# defined in _bookdown.yml
all: gitbook pdf

gitbook:
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::gitbook')"

pdf:
	Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::pdf_book')"

serve:
	Rscript -e "bookdown::serve_book(in_session = TRUE, preview=FALSE, port=5656)"

clean:
	Rscript -e "bookdown::clean_book(TRUE)"
	rm -rf _bookdown_files
	rm -f *.log

install_requirements:
	Rscript install_requirements.R


